

const $root = $('html, body');
const $upScrollBtn = $('.btn-up');
const scrollTrigger = window.innerHeight;


$('.menu-link').click(function (e) {
    e.preventDefault();
    console.log(this)
    $root.animate({
        scrollTop: $($(this).attr('href')).offset().top
    }, 500)
});


$('.btn-slide').click(() => {
    $('.posts-gallery').slideToggle(1000)
});


$(window).scroll(() => {
    if ($root.scrollTop() >= scrollTrigger) {
        $upScrollBtn.show()
    } else {
        $upScrollBtn.hide()
    }
});

$upScrollBtn.click(() => {
    $root.animate({
        scrollTop: 0
    }, 500)
})


